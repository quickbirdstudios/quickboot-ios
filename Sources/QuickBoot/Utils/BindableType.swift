//
//  BindableType.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 29.06.17.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RxSwift

public protocol BindableType: AnyObject {
    associatedtype ViewModelType

    var viewModel: ViewModelType! { get set }

    func bindViewModel()
}

extension BindableType where Self: UIViewController {

    public func bind(to model: Self.ViewModelType) {
        viewModel = model
        loadViewIfNeeded()
        bindViewModel()
    }

}

extension BindableType where Self: UITableViewCell {

    public func bind(to model: Self.ViewModelType) {
        viewModel = model
        bindViewModel()
    }

}

extension BindableType where Self: UICollectionViewCell {

    public func bind(to model: Self.ViewModelType) {
        viewModel = model
        bindViewModel()
    }

}
