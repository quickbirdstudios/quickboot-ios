//
//  AlertViewModel.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RxSwift

public struct AlertItem<Item: CustomStringConvertible> {
    public let style: UIAlertAction.Style
    public let item: Item

    public static func `default`(_ item: Item) -> AlertItem<Item> {
        return AlertItem<Item>(style: .default, item: item)
    }

    public static func cancel(_ item: Item) -> AlertItem<Item> {
        return AlertItem<Item>(style: .cancel, item: item)
    }

    public static func destructive(_ item: Item) -> AlertItem<Item> {
        return AlertItem<Item>(style: .destructive, item: item)
    }
}

public class AlertViewModel<Item: CustomStringConvertible> {

    // MARK: Output
    public lazy var selectedItem: Observable<Item> = selectedItemSubject.asObservable()

    // MARK: Internals
    let title: String?
    let message: String?
    let items: [AlertItem<Item>]
    let selectedItemSubject = PublishSubject<Item>()

    // MARK: - Init

    public init(title: String?, message: String?, items: [AlertItem<Item>]) {
        self.title = title
        self.message = message
        self.items = items
    }

}
