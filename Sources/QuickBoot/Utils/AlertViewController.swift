//
//  AlertViewController.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit

public class AlertViewController<Item: CustomStringConvertible>: UIAlertController {

    var viewModel: AlertViewModel<Item>!

    public convenience init(preferredStyle: UIAlertController.Style, viewModel: AlertViewModel<Item>) {
        self.init(title: nil, message: nil, preferredStyle: preferredStyle)
        self.viewModel = viewModel

        bindViewModel()
    }

    // MARK: BindableType

    func bindViewModel() {
        self.title = viewModel.title
        self.message = viewModel.message

        for item in viewModel.items {
            let action = UIAlertAction(title: item.item.description, style: item.style, handler: { [weak self] _ in
                self?.viewModel.selectedItemSubject.onNext(item.item)
            })
            addAction(action)
        }
    }

}
