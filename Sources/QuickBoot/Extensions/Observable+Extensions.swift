//
//  Observable+Extensions.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import RxSwift

public protocol OptionalType {
    associatedtype Wrapped
    var value: Wrapped? { get }
}

extension Optional: OptionalType {
    public var value: Wrapped? {
        return self
    }
}

extension ObservableType where Element: OptionalType {

    public func skipNil() -> Observable<Element.Wrapped> {
        return self.filter { $0.value != nil }.map { $0.value! }
    }

}

extension ObservableType where Element: Collection {

    public func mapElements<T>(_ transform: @escaping (Self.Element.Element) -> T) -> Observable<[T]> {
        return self.map { collection -> [T] in
            collection.map(transform)
        }
    }

    public func mapElementsEnumerated<T>(_ transform: @escaping (Int, Self.Element.Element) -> T) -> Observable<[T]> {
        return self.map { collection -> [T] in
            collection.enumerated().map(transform)
        }
    }

    public func mapElements<T>(keyPath: KeyPath<Self.Element.Element, T>) -> Observable<[T]> {
        return self.map { collection -> [T] in
            collection.map(keyPath: keyPath)
        }
    }

    public func filterElements(_ condition: @escaping (Self.Element.Element) -> Bool) -> Observable<[Self.Element.Element]> {
        return self.map { collection -> [Self.Element.Element] in
            collection.filter(condition)
        }
    }

}
