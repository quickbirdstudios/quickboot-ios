//
//  String+Validators.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation

extension String {

    public func isEmailValid() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }

}
