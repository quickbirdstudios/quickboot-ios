//
//  Collection+KeyPath.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation

extension Collection {

    public func map<T>(keyPath: KeyPath<Element, T>) -> [T] {
        return self.map { value in
            return value[keyPath: keyPath]
        }
    }

    public func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>, ascending: Bool = true) -> [Element] {
        return sorted(by: { element1, element2 -> Bool in
            if ascending {
                return element1[keyPath: keyPath] < element2[keyPath: keyPath]
            } else {
                return element1[keyPath: keyPath] > element2[keyPath: keyPath]
            }
        })
    }

}
