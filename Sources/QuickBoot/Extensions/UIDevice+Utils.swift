//
//  UIDevice+Extras.swift
//  Schultopf
//
//  Created by Paul Kraft on 13.07.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit

extension UIDevice {

    public var isSmallDevice: Bool {
        return UIScreen.main.bounds.size.width <= 320
    }

    public var isSimulator: Bool {
        #if targetEnvironment(simulator)
            return true
        #else
            return false
        #endif
    }

}

