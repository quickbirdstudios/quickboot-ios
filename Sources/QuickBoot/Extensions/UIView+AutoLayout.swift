//
//  UIView+AutoLayoutHelpers.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit

extension UIView {

    public func constrainEdges(toEdgesOf view: UIView, insets: UIEdgeInsets = .zero) {
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: view.topAnchor, constant: insets.top),
            leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: insets.left),
            trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -insets.right),
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -insets.bottom),
        ])
    }

    public func center(inView view: UIView, offset: CGSize = .zero) {
        NSLayoutConstraint.activate([
            centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: offset.width),
            centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: offset.height),
        ])
    }

}

extension NSLayoutConstraint {

    public func activate() {
        NSLayoutConstraint.activate([self])
    }

    public func deactivate() {
        NSLayoutConstraint.deactivate([self])
    }

}
