//
//  UIButton+BackgroundColor.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit

extension UIButton {

    public func setBackgroundColor(_ backgroundColor: UIColor, for controlState: UIControl.State) {
        let image = UIImage(color: backgroundColor)
        self.setBackgroundImage(image, for: controlState)
    }

}
