#
# Be sure to run `pod lib lint QuickBoot.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'QuickBoot'
    s.version          = '0.2.0'
    s.summary          = 'QuickBird Studios iOS Base Library.'

    s.description      = <<-DESC
    TODO: Add long description of the pod here.
    DESC

    s.homepage         = 'https://github.com/quickbirdstudios/quickboot-ios'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'grafele' => 'stefan.kofler@quickbirdstudios.com' }
    s.source           = { :git => 'https://github.com/quickbirdstudios/quickboot-ios', :tag => s.version.to_s }

    s.ios.deployment_target = '9.0'
    s.swift_version = '5.0'

    s.source_files = 'Sources/QuickBoot/**/*'

    s.framework  = 'Foundation'
    s.framework  = 'UIKit'
    s.dependency 'RxSwift', '~> 5.0'
    s.dependency 'RxCocoa', '~> 5.0'
end
