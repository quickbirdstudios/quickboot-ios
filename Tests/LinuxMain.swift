import XCTest

import QuickBootTests

var tests = [XCTestCaseEntry]()
tests += QuickBootTests.allTests()
XCTMain(tests)
